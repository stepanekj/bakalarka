\chap {Implementace}

V této kapitole demonstruji použití systému Puppet při správě počítačové sítě s počítači s operačním systémem
Windows. Zaměřím se zejména na pracovní postup vývoje konfigurace a na představení zdrojového kódu konfigurace.

\sec {Specifikace úlohy}

Představím modelový příklad, na kterém demonstruji základní techniky a přednosti systému Puppet.

Mějme počítačovou učebnu, ve které probíhá výuka programování. Potřebujeme zajistit, aby na počítačích
byly nainstalovány programy pro výuku (Notepad++ a NetBeans), aby se mohli studenti na počítače přihlásit -- tedy aby každý měl svého
uživatele. Na konci výuky bude pro studenty připraven test -- je tedy třeba na stroje rozdistribuovat
zadání. Aby ale studenti neopisovali, budou jejich stroje předem rozděleny do dvou skupin a každá
skupina dostane zadání odlišné.

\sec {Výběr technologií}

Veškeré zdrojové kódy budu udržovat v git repositáři umístěném ve veřejné službě BitBucket \url{git@bitbucket.org:stepanekj/puppet.git}.

Na základě zhodnocení prostředí AWS z kapitoly Testovací prostředí, jsem se rozhodl použít právě to. Kromě již zmíněných
výhod platformy AWS je pozitivní i podpora Puppetu. Puppet nabízí mnoho modulů pro ovládání prvků AWS infrastruktury~\cite[puppet-aws].
Téměř celou konfiguraci prostředí tak mohu udržovat jako Puppet manifest (s výjimkou dynamického registrování strojů do dns Route53).

Jako distribuční model pro tuto úlohu použiji model "agent - master" -- tedy bude zapotřebí jeden linuxový stroj
 pro roli Puppet Mastera a dále spravované stroje s operačním systémem Windows. Pro mastera použiji operační systém
 Debian ve verzi 8.4 (kódové označení Jessie) a pro agenty použiji operační systém Microsoft Windows Server 2012 R2 Base.

 Dále budu samozřejmě potřebovat program "puppet" -- zvolil jsem verzi 3.7.2 zejména proto, že je obsažena ve výchozím
 nastavení Debianu.

\sec {Vývojové prostředí}

\midinsert  \clabel[dev-environment]{Diagram vývojového prostředí v AWS}
\picw=15cm \cinspic img/dev_environment.pdf
\caption/f Diagram vývojového prostředí v AWS.
\endinsert

Na obrázku \ref[dev-environment] je znázorněna struktura vývojového prostředí. Celé prostředí je izolováno ve vlastním VPC. Uvnitř VPC je
definována lokální podsíť, ke které je připojena IGW, aby stroje mohly komunikovat s vnějším internetem. V síti jsou
dále umístěny EC2 instance, ke kterým se budu připojovat pomocí ElasticIP. Stroje dostávají při každém spuštění jinou vnitřní
IPv4 adresu, ale my musíme zanést propojení strojů do neměnné struktury zdrojového kódu --
potřebujeme proto předklad ip adres na stálé dns záznamy. To zajistí služba Route53 -- dynamicky
konfigurovatelný dns server. Bohužel pro dynamické registrování EC2 instancí do Route53 dns serveru zatím Puppet modul není.
Při zakládání prostředí jsem tedy zadal příslušné záznamy do Route53 ručně. Dalo by se to jistě zautomatizovat
použitím samostatného nástroje, který přihlašuje systém do Route53 při startu a odhlašuje při vypínání.

Dále představím vlastní konfiguraci AWS vývojového prostředí zapsanou jako Puppet Manifest:

  \begtt

$vpcName = 'agent-master'
$ensure = 'present'

$debianMasterName = 'debian-master'
$debianMasterAmi = 'ami-4ebd5221'

aws_vpc { $vpcName:
  ensure  => $ensure,
  vpcName => $vpcName,
}

aws_vpc::ec2_instance { 'debian-master':
  name     => $debianMasterName,
  image_id => $debianMasterAmi,
}


Aws_vpc::Ec2_instance <||> {
  ensure => $ensure,
  subnet => "${vpcName}-subnet",
  security_groups => ["${vpcName}-access"]
}

aws_vpc::ec2_elastic_ip { '52.28.187.150':
  ensure   => $ensure,
  instance => $debianMasterName
}

if $ensure == 'absent' {
  Ec2_instance <||> {
    before => [
      Ec2_vpc_subnet["${vpcName}-subnet"],
      Ec2_securitygroup["${vpcName}-access"]
    ]
  }
}

  \endtt

Pro jednoduchost zde uvádím příklad vytvoření jedinného stroje Puppet Master - jak se vytváří více strojů je uvedeno v repositáři
práce. Typ "Aws_vpc" je můj vlastní modul, který zapouzdřuje ovládání VPC. Obsahuje definované typy pro vytvoření síťové
infrastruktury, pro vytvoření EC2 instance a pro připojování k ElasticIP.

Tento manifest sestaví celé prostředí podle specifikovaných parametrů. Je sestaven tak, aby protředí uměl vytvořit
i odstranit pomocí proměnné "ensure". Je důležité implementovat i odstraňování, abychom mohli po otestování nepoužívané
prostředky z aws odstranit (čas po který jsou prostředky spuštěny je zpoplatněn).

Z kódu je patrné, že na sobě jednotlivé zdroje závisí. Pro zapojení EC2 instance
musí být například již připravena podsíť. Aplikují se zde {\em implicitní závislosti}. To je ovšem problém při
odstraňování pomocí "ensure => absent" -- tehdy je třeba odstraňovat zdroje v opačném pořadí, než při vytváření.
Implicitní závislosti totiž neřeší, zda zdroje vytváříme nebo odstraňujeme a aplikuje se vždy.Při odstraňování
je ovšem třeba závislosti obrátit. Například nejprve je třeba odtranit EC2 instanci a teprve potom můžeme
rušit síť. Proto je na konci manifestu výraz v podmínce "if(ensure => absent)", který pořadí provádění zdrojů
upravuje explicitně.

Dále je třeba zmínit AMI šablony, ze kterých jsou EC2 instance spouštěny. Ty jsem předem připravil tak,
že jsem na čistý operační systém nainstaloval program "puppet", resp. "puppetmaster". To je první nutný
krok, abychom s Puppet infrastrukturou vůbec mohli začít. Dále jsem do AMI obrazu pro Puppet Mastera
nainstaloval program "git" a přidal \uv{deploy} ssh klíč, abych na něj mohl pomocí Gitu nahrát zdrojový kód konfigurace
z git repositáře.

Manifest je možné spustit na libovolném počítači, ze kterého se můžeme připojit do našeho AWS účtu
pomocí "aws cli" programu. Puppet moduly vyžadují pro přihlášení nastavené proměnné prostředí s přihlašovacími
údaji k AWS účtu "AWS_ACCESS_KEY_ID" a "AWS_SECRET_ACCESS_KEY", které nalezneme ve webovém administračním rozhraní AWS.


\secc {Psaní Puppet kódu}

Paralelně s vytvářením vývojového prostředí můžeme začít připravovat manifesty i pro hlavní konfiguraci
Windows. Vývoj konfigurace probíhá na lokálním počítači pomocí oblíbeného vývojového editoru. Pro mnoho
programátorských editorů existují rozšíření pro jazyk Puppet. Při psaní kódu si již na lokálním počítači
můžeme postupně oveřovat správnost konfigurace pomocí spouštění "pupet lint" (statická kontrola Puppet kódu) nebo
například psaním unit testů v prostředí "rspec-puppet".

\secc {Navázání komunikace agentů s masterem}

Pro zahájení komunikace mezi agenty a masterem je třeba provést mezi těmito stroji výměnu a potvrzení ssl certifikátů.
Komunikace totiž probíhá zabezpečeně. Každý agent i master si při prvním spuštění vygeneruje dvojici klíčů.
Agenti při navázání spojení pošlou k ověření svůj veřejný klíč masterovi a ten si klíč zařadí do skupiny klíčů
k ověření. Administrátor potom tyto požadavky na masterovi ověří zkontrolováním otisku klíče a následným
příkazem "puppet cert sign <hostname agenta>" potvrdí autenticitu certifikátu. Agentovi se vrátí zpět veřejný klíč mastera -- potvrzovat nemusí
nic.

Tímto se spustí proces správy konfigurace agenta.

\secc {Nasazení kódu na vývojové prostředí}

Kód udržovaný v git repositáři potřebujeme nahrát na Puppet Master. Použiji k tomu program "r10k".
Do konfiguračního souboru "/etc/r10k/r10k.yml" na stroji Puppet Master zadáme cestu ke git repositáři a cestu kam chceme kód
umístit (například přímo do "/etc/puppet" ,kde ho Puppet očekává). Spuštěním příkazu \begtt r10k deploy environment -p\endtt
na stroji Puppet Master zařídíme nahrání aktuální verze kódu z git repositáře do dané cesty. Program "r10k" zároveň
vytvoří správnou adresářovou strukturu v "/etc/puppet/environment". Pro každou větev v git repositáři připraví
vlastní adresář a tím může Puppet pracovat s větvemi gitu jako s Puppet Environments -- toho využijeme při
nahrávání odlišných verzí zadání testu pro studenty na výukové stroje. V konfiguraci Puppetu každého výukového
stroje zadáme jedno ze dvou prostředí a pomocí větví v gitu potom udržujeme rozdíly mezi těmito konfiguracemi.

\sec {Výsledná konfigurace}

Úlohu jsem naimplementoval pomocí tří modulů. Ovládají uživatele, instalaci programů a soubory se zadáním.

\secc {Modul students}

Modul "students" má jeden argument a to seznam studentů. Podle tohoto argumentu založí příslušné lokální uživatele,
nejdřív ovšem zařídí, aby existovala potřebná skupina uživatelů "students". Každému uživateli modul založí vlastní
domovský adresář. Bohužel zdroj "user" neumí adresář založit sám a spoléhá se, že již existuje. Proto adresář musíme
založit sami, pomocí zdroje "file".

\begtt
class students(
  $students = []
){
  # local user and local group
 each($students)|$student| {

    file { "C:\\Users\\${student}":
      ensure => directory
    }
    user { $student:
      ensure => present,
      password => '123456Abc',
      groups => ['students', 'Remote Desktop Users'],
      home => "C:\\Users\\${student}"
    }
  }
  group { 'students':
    ensure => present
  }
}
\endtt



\secc {Modul classroom\_programs}

Pro správu programů použiji správce balíků {\em chocolatey}. Pomocí něj je instalace  programů velice snadná --
 stačí použít zdroj "package"

 \begtt
class classroom_programs{
  include 'chocolatey'

  package{ 'notepadplusplus.install':
    ensure => latest,
    provider => chocolatey
  }
  package{ 'netbeans':
    ensure => '7.3',
    provider => chocolatey,
    require => Package['jdk8']
  }
  package{'jdk8':
    ensure => '8.0.92',
    provider => chocolatey
  }
}
 \endtt

 Všimněme si, že jsem zde musel vyjádřit závislost programu "NetBeans" na "jdk8" -- Puppet o této závislosti nic neví
 a proto zde nenastane {\em automatický require}.

\secc {Modul classroom\_test}

Nakonec je třeba na vzdálené stroje nahrát zadání úkolu. K tomu nám poslouží modul "classroom\_test", který díky parametrizované
šabloně vyplní zadání pro každého studenta individuálně.

\begtt
class classroom_test(
  $students
){

  each($students)|$student| {

    file { "C:\\Users\\${student}\\test.txt":
      ensure => file,
      require => File["C:\\Users\\${student}"],
      content => template('classroom_test/test.txt.erb')
    }
  }

}
\endtt

Šablona pro zadání s použitým parametrem:

\begtt
Zadání testu A pro <\%=@student\%>
\endtt

Dále mají být stroje rozděleny do dvou skupin tak, aby každá skupina dostala odlišné zadání. To můžeme elegantně zařídit
použitím Puppet Environments. V git repositáři k tomu účelu připravíme dvě větve "zadaniA" a "zadaniB". Na Puppet Masteru
potom pomocí příkazu \begtt r10k deploy environment -p\endtt vytvoříme následující adresářovou strukturu:

\begtt
/etc/puppet/environments
/etc/puppet/environments/zadaniA
/etc/puppet/environemnts/zadaniB
\endtt

To je přesně struktura, které Puppet Master rozumí a může tak agentům poskytovat dvě různé verze manifestů. V našem případě
se manifesty liší práve v šabloně zadání.